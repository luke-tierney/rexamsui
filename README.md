# Using R/exams on ICON

## Basics

[R/exams](http://www.r-exams.org/) supports using R to create exams
and quizzes for a number of learning management systems (LMS),
including [Canvas](http://www.r-exams.org/), the system ICON is based
on.

Support for Canvas seems more limited than for other LMS but may still
be useful. For me at least building a quiz by writing RMarkdown files
is probably easier and less error-prone than clicking around the ICON
web interface for building quizzes.


## Supported Question Types and Features

The question types R/exams question types that are supported on
Canvas, and their Canvas counterparts are

| R/exams   |  Canvas            |
|-----------|--------------------|
|`schoice` |  Multiple Choice   |
|`mchoice` |  Multiple Answers  |
|`num`     |  Numerical Answer  |
|`string`  |  Fill in the Blank |

It is possible to generate a random data file and include it with a
question, but it does not seem to be possible for multiple questions
to share a data file.

Math is supported; I'm not sure if it works on all browsers.

Exams can be written in Rmarkdown or Rnw syntax; I have only tried
Rmarkdown.

Adding solution explanations does not seem to work properly in the
current `exams` package. It looks like it is close but not quite
there.


## Trying It Out

The directory `quiz` contains some sample questions illustrating the
four question types. In addition, `lm.Rmd` shows how to generate and
include a data file and `hist.Rmd` shows including a graph.

You can create an HTML preview of a quiz based on these files with

```r
library(exams)
exams2html(dir("quiz", full.names = TRUE), name = "RexamsUI")
```

To create a quiz file for upload to ICON you can use

```r
exams2canvas(dir("quiz", full.names = TRUE), name = "RexamsUI")
```

This creates a QTI .zip file `RexamsUI.zip`.

You can upload this quiz to your ICON class and try it out. I
recommend starting in the _test_ version of your class: If your class
instance is at

<https://uiowa.instructure.com/courses/154662>

then the test version is at

<https://uiowa.test.instructure.com/courses/154662>

These are the steps:

* Click in  _Settings_ (left bar)
* Click on _Import Course Content_ (right list)
* In the _Content Type_ drop-down choose _QTI .zip file_.
* Click _Browse_ and upload your file.

This seems to add a new quiz under **Quizzes** named **RexamsUI** and
also add the questions to your question bank.

That's about as much as I know at this point.

I learned how to get things into Canvas from [this post](https://kenjokenjo.wordpress.com/2020/08/23/creating-canvas-quizzes-and-exams-using-r/).

Another [useful
post](https://community.canvaslms.com/t5/Question-Forum/Partial-credit-for-multiple-answer-questions/td-p/207296)
explaining how Canvas handled partial credit for _Multiple Answers_
(`mchoice`) questions.

Question
========

Consider the following histogram:

```{r, echo = FALSE, fig.cap = ""}
n <- 200
x <- rchisq(n, 3)
hist(x)
```

Which of the following statements are true?


Answerlist
----------
* The distribution is skewed right.
* The distribution is skewed left.
* The distribution is symmetric.
* The mean is larger than the median.
* The mean is smaller than the median.
* The mean and median are about the same.


Meta-information
================
exname: Example of a question with a graph.
extype: mchoice
exsolution: 100100
exshuffle: TRUE

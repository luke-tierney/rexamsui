<!-- adapted from lm.Rmd in package exams -->
```{r data generation, include = FALSE}
d <- data.frame(x = runif(100, -1, 1))
a <- 0
b <- sample(c(-1, 1), 1) * sample(c(0, 0.6, 0.9), 1)
d$y <- a + b * d$x + rnorm(100, sd = 0.25)
write.csv(d, "regression.csv", row.names = FALSE, quote = FALSE)

m <- lm(y ~ x, data = d)
bhat <- coef(m)[2]
```

Question
========
Using the data provided in [regression.csv](regression.csv) fit a
linear regression of `y` on `x`. What is the estimated slope with
respect to `x`?


Meta-information
================
exname: Linear regression
extype: num
exsolution: `r fmt(bhat, 3)`
extol: 0.01
